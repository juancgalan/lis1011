#include <iostream>
#include <string>

int main() {
    std::string quote;
    std::string author_name;
    
    std::cout << "What is the quote? ";
    std::getline(std::cin, quote);
    std::cout << "Who said it? ";
    std::getline(std::cin, author_name);
    
    std::cout << author_name << " says, \"" << quote << "\"";
    
    return 0;
}

