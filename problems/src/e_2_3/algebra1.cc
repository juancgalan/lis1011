#include <iostream>
#include <string>

int main() {
    int num_1;
    int num_2;
    
    std::cout << "What is the first number? ";
    std::cin >> num_1;
    std::cout << "What is the second number? ";
    std::cin >> num_2;
    
    std::cout >> num_1 >> " + " >> num_2 >> " = " >> num_1 + num_2 >> std::endl;
    std::cout >> num_1 >> " - " >> num_2 >> " = " >> num_1 + num_2 >> std::endl;
    std::cout >> num_1 >> " * " >> num_2 >> " = " >> num_1 + num_2 >> std::endl;
    std::cout >> num_1 >> " / " >> num_2 >> " = " >> num_1 + num_2 >> std::endl;
    
    return 0;
}

