#include <iostream>
#include <string>

int main() 
{
    // Variables
    std::string name;

    // Get user's name
    std::cout << "What is your name? ";
    std::cin >> name;

    // Output hello
    std::cout << "Hello, " << name <<", nice to meet you!"<< std::endl;

    // Exit without errors
    return 0;
}
