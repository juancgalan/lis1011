#include <iostream>
#include <string>

int main() 
{
    // Variables
    std::string input;
    
    // Get input string
    std::cout << "What is the input string?";
    std::cin >> input;
    
    // Output length
    std::cout << input << " has " << input.size() << " characters." << std::endl;

    // Exit without errors
    return 0;
}
