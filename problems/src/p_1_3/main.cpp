#include <iostream>
#include <string>

int main()
{
    // Variables
    std::string quote;
    std::string name;

    // Get name and quote
    std::cout << "What is the quote?";
    std::cin >> quote;
    std::cout << "Who said it?";
    std::cin >> name;

    // Print quote
    std::cout << name << " says, \"" << quote << "\"";

    // Exit without errors
    return 0;
}
