#include <iostream>
#include <string>

int main()
{
    // Variables
    std::string noun;
    std::string verb;
    std::string adjective;
    std::string adverb;

    // Get all required data
    std::cout << "Enter a noun:";
    std::cin >> noun;
    std::cout << "Enter a verb:";
    std::cin >> verb;
    std::cout << "Enter a adjective:";
    std::cin >> adjective;
    std::cout << "Enter a adverb:";
    std::cin >> adverb;
    
    // Output phrase
    std::cout << "Do you " << verb << " your " << adjective << " " << noun << " " << adverb << "? That's hilarious!";

    // Exit without errors
    return 0;
}
