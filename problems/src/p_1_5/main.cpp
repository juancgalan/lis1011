#include <iostream>
#include <string>

int main()
{
    // Variables
    int first_number;
    int second_number;
    int add;
    int minus;
    int by;
    int over;

    // Get numbers
    std::cout << "What is the first number?";
    std::cin >> first_number;
    std::cout << "What is the second number?";
    std::cin >> second_number;

    // Calculate results
    add = first_number + second_number;
    minus = first_number - second_number;
    by = first_number * second_number;
    over = first_number / second_number;

    // Print results
    std::cout << first_number << " + " << second_number << " = " << add << std::endl;
    std::cout << first_number << " - " << second_number << " = " << minus << std::endl;
    std::cout << first_number << " * " << second_number << " = " << by << std::endl;
    std::cout << first_number << " / " << second_number << " = " << over << std::endl;

    // Exit without errors
    return 0;
}
