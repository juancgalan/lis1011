#include <iostream>

int main() 
{
    // Variables
    int current_age = 0;
    int retirement_age = 0;
    int age_difference = 0;
    int current_year = 0;
    int retirement_year = 0;

    // Ask for age
    std::cout << "What is your current age?" << std::endl;
    std::cin >> current_age;
    std::cout << "At what age would you like to retire?" << std::endl;
    std::cin >> retirement_age;
    std::cout << "What is the current year?" << std::endl;
    std::cin >> current_year;
    
    // Calculate age difference
    age_difference = retirement_age - current_age;
    std::cout << "You have " << age_difference << " years left until you can retire." << std::endl;
    
    // Calculate retirement year
    retirement_year = current_year + age_difference;
    std::cout << "It's " << current_year << ", so you can retire in " << retirement_age << "." << std::endl;
    
    // Exit without errors
    return 0;
}
