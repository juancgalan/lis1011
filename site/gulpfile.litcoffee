# Build System for a static site based on Hugo
================================================================================

This is the build system for the main site of the lecture
[LIS1011](http://lis1011.juancgalan.com).

  - Run `gulp build:sass` to launch the sass to css translation.
  - Run `gulp build:hugo:all` to build hugo static site with drafts.
  - Run `gulp build:hugo` to build hugo static site without drafts.
  - Run `gulp build:all` to build both hugo with drafts and sass.
  - Run `gulp serve` to run a local web server on the project.
  - Run `gulp serve --watch` to run a local web server and auto build on
    changes.

Dependencies
--------------------------------------------------------------------------------

    gulp = require 'gulp'
    sass = require 'gulp-sass'
    mainBowerFiles = require 'main-bower-files'
    exists = require('path-exists').sync
    gutil = require 'gulp-util'
    del = require 'del'
    path = require 'path'
    exec = require 'gulp-exec'
    connect = require 'vinyl-paths'
    argv = require('yargs').argv

Source files and folders
--------------------------------------------------------------------------------

    source = {}
    source.package = 'package.json'
    source.publicDir = './public'
    source.hugoDir = './hugo'
    source.hugoThemesDir = source.hugoDir + '/themes'
    source.hugoStaticDir = source.hugoDir + '/static'
    source.hugoTheme = 'hugo-lecture'
    source.hugoPort = 1313
    source.hugoThemeDir = source.hugoThemesDir + '/' + source.hugoTheme
    source.hugoThemeSassDir = source.hugoThemeDir + '/sass'
    source.hugoThemeLayoutDir = source.hugoThemeDir + '/layouts'
    source.hugoThemeStaticDir = source.hugoThemeDir + '/static'

Destination files and folders
--------------------------------------------------------------------------------

    destination =
        public: './public'
        themeJs: source.hugoThemeStaticDir + '/js'
        themeCss: source.hugoThemeStaticDir + '/css'
        hugoThemeCssDir: source.hugoThemeStaticDir + '/css'

Web Server Options
--------------------------------------------------------------------------------

    serverOptions =
        root: destination.public
        livereload: true
        port: source.hugoPort

Build:sass task
--------------------------------------------------------------------------------

    gulp.task 'build:sass', ->
        gutil.log 'Writing compiled sass on ' + destination.hugoThemeCssDir
        gulp.src [source.hugoThemeSassDir + '/**/*.sass'], ['/bower_components/foundation-sites/scss/**/*.scss']
            .pipe sass({includePaths: ['bower_components/foundation-sites/scss']})
            .on 'error', sass.logError
            .pipe gulp.dest(destination.hugoThemeCssDir)
