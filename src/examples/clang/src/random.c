#include <stdio.h>
#include <limits.h>

int main()
{
    long seed = 8; // Cambia este para una nueva secuencia
    int a = 1664525;
    int c = 12345;
    int m = INT_MAX;

    printf("Input seed:");
    scanf("%li", &seed);
    for (int i = 1; i <= 10; i++) {
	seed = (a * seed + c) % m;
	printf("%li\n", seed);
    }
}
