#+LATEX_CLASS: koma-article
#+LATEX_CLASS_OPTIONS: [BCOR=0mm, DIV=11, headinclude=false, footinclude=false, paper=A4, fontsize=8pt,twoside]
#+LATEX_HEADER: \usepackage{format/header}
#+TITLE:
#+OPTIONS: H:1 toc:nil
#+HTML_DOCTYPE:

#+BEGIN_EXPORT latex
\renewcommand{\thecareer}{Bachelor in Computer Science and Information Technology}
\renewcommand{\thedocumenttitle}{Week 6}
\renewcommand{\theterm}{Fall 2017}
\renewcommand{\thecoursename}{Introduction to Programming}
\renewcommand{\thecoursecode}{LIS1011}
\makeheadfoot
#+END_EXPORT

* Purpose
This document describes the content and activities for Week 5 of the course
*Introduction to Programming* with code *LIS1011*.  This week we will
continue with arrays. A special case of arrays is when they are of type
~char~. An array of ~chars~ is called a /string/. In this week you will
learn how to declare and manipulate strings.

* Outline
  1. String Declaration.
  2. String Input.
  3. String Output.
  4. String Functions.
  5. Character Functions.
  6. String to number.
  7. Command Line Arguments (~argc~, ~argv~).

* Instructor-led Training Activities
  Note: Led-Training Activities are described on the Lab Practice document.

* TODO Self-pace Learning Activities
  1. Watch the following videos

  2. Read the following material

* Assignment: Manipulating Strings
   1. What’s wrong with this attempted declaration of a character string?
      #+begin_src c :export code
	int main(void)
	{
	    char name[] = {'F', 'e', 's', 's' };
	    ...
	}
      #+end_src
   2. What will this program print?
      #+begin_src c :export code
	#include <stdio.h>

	int main(void)
	{
	    char note[] = "See you at the snack bar.";
	    char *ptr;
	    ptr = note;
	    puts(ptr);
	    puts(++ptr);
	    note[7] = '\0';
	    puts(note);
	    puts(++ptr);
	    return 0;
	}
      #+end_src
   3. Write a program that reads input until encountering # . Have the program
      print each input character and its ASCII decimal code. Print eight
      character-code pairs per line.  Suggestion: Use a character count and the
      modulus operator ( % ) to print a newline character for every eight cycles
      of the loop.
   4. Using if else statements, write a program that reads input up to # ,
      replaces each period with an exclamation mark, replaces each exclamation
      mark initially present with two exclamation marks, and reports at the end
      the number of substitutions it has made.
   5. The following provides practice with strings, loops, pointers, and pointer
      incrementing. First, suppose you have this function definition:
      #+begin_src c :export code
	#include <stdio.h>

	char *pr (char *str)
	{
	    char *pc;
	    pc = str;
	    while (*pc)
		putchar(*pc++);
	    do {
		putchar(*--pc);
	    } while (pc - str);
	    return (pc);
	}
      #+end_src
      Consider the following function call: ~x = pr("Ho Ho Ho!");~
      1. What is printed?
      2. What type should x be?
      3. What value does x get?
      4. What does the expression *--pc mean, and how is it different from --*pc?
      5. What would be printed if *--pc were replaced with *pc--?
      6. What do the two while expressions test for?
      7. What happens if pr() is supplied with a null string as an argument?
      8. What must be done in the calling function so that pr() can be used as
	 shown?
   6. Design and test a function that reads the first word from a line of input
      into an array and discards the rest of the line. It should skip over
      leading whitespace. Define a word as a sequence of characters with no
      blanks, tabs, or newlines in it. Use getchar(), not
   7. Write a function that replaces the contents of a string with the string
      reversed. Test the function in a complete program that uses a loop to
      provide input values for feeding to the function.
   8. Write a program that echoes the command-line arguments in reverse word
      order. That is, if the command-line arguments are see you later, the
      program should print later you see.
   15. Use the character classification functions to prepare an implementation of
       atoi(); have this version return the value of 0 if the input string is
       not a pure number.

* TODO Assessment

  1. *Due Date: Jun 17, Thursday*
  2. Grading
	\begin{equation}
	grade = \begin{cases}
		x\mbox{ if } x \leq 10\mbox{ where }x = \frac{A + 2B + 3C}{18} \times 10 \geq 10\\
		10\mbox{ otherwise }
		\end{cases}
	\end{equation}
	\begin{equation}
	extra = 1 if C \geq 4
	\end{equation}
  4. Assesment Reference.

#+ATTR_LATEX: :environment tabularx :width \textwidth :align |p{1in}|X|X|X|X|X|
      |----------------------+----------------------+----------------------+----------------------+----------------------+----------------------|
      | <20>                 | <20>                 | <20>                 | <20>                 | <20>                 | <20>                 |
      |                      | 1. Unnaceptable      | 2. Basic             | 3.  Proficient       | 4.  Exceed Expectations | 5. Distinguished     |
      |----------------------+----------------------+----------------------+----------------------+----------------------+----------------------|
      | A. Due Date          | Sent one week later. | Sent three days later. | Sent on time.        | Sent two days before. | Sent four days before. |
      |----------------------+----------------------+----------------------+----------------------+----------------------+----------------------|
      | B. Creativity        | Submission is a copy of another application on the Internet | Submission is a copy of another application on the Internet with slight changes. | Submission is an original idea with some elements from tutorials and examples | Submission is complety original and low on complexity | Submission is an original idea and high on complexity |
      |----------------------+----------------------+----------------------+----------------------+----------------------+----------------------|
      | C. Quality of Response | Application is incomplete and only covers one element of the requested conditions | Applitaction is not fully complete | Application is complete | Application includes some extra low complexity elements | Application includes extra high complexity elements |
      |----------------------+----------------------+----------------------+----------------------+----------------------+----------------------|
