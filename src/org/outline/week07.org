#+LATEX_CLASS: koma-article
#+LATEX_CLASS_OPTIONS: [BCOR=0mm, DIV=11, headinclude=false, footinclude=false, paper=A4, fontsize=8pt,twoside]
#+LATEX_HEADER: \usepackage{format/header}
#+TITLE:
#+OPTIONS: H:1 toc:nil
#+HTML_DOCTYPE:

#+BEGIN_EXPORT latex
\renewcommand{\thecareer}{Bachelor in Computer Science and Information Technology}
\renewcommand{\thedocumenttitle}{Week 7}
\renewcommand{\theterm}{Fall 2017}
\renewcommand{\thecoursename}{Introduction to Programming}
\renewcommand{\thecoursecode}{LIS1011}
\makeheadfoot
#+END_EXPORT

* Purpose
This document describes the content and activities for Week 5 of the course
*Introduction to Programming* with code *LIS1011*. On this week you will
learn how to declare your own functions. We will discuss the syntax for
creating different types of functions.

* Outline
  1. Function Declaration.
  2. Function Arguments.
  3. Returned value.
  4. Function Prototyping.
  5. Function Composition.

* Instructor-led Training Activities
  Note: Led-Training Activities are described on the Lab Practice document.

* TODO Self-pace Learning Activities
  1. Watch the following videos

  2. Read the following material

* Assignment: Simple Functions
   1. Type in and run the 16 programs presented in the provided chapter 8 of
      /Programming in C/ by /Stephen G. Kochan/. Compare the output produced by
      each program with the output presented after each program in the text.
   2. Modify Program 8.4 so the value of triangularNumber is returned by the
      function.Then go back to Program 5.5 and change that program so that it
      calls the new version of the calculateTriangularNumber function.
   3. Modify Program 8.8 so that the value of epsilon is passed as an argument
      to the function.Try experimenting with different values of epsilon to see
      the effect that it has on the value of the square root.
   4. Modify Program 8.8 so that the value of guess is printed each time through
      the while loop. Notice how quickly the value of guess converges to the
      square root.  What conclusions can you reach about the number of
      iterations through the loop, the number whose square root is being
      calculated, and the value of the initial guess?
   8. An equation of the form $ax^2 + bx + c = 0$ is known as a quadratic
      equation.  The values of a, b, and c in the preceding example represent
      constant values. So $4x^2 - 17x - 15 = 0$ represents a quadratic equation
      where $a = 4$, $b = –17$, and $c = –15$.The values of $x$ that satisfy a
      particular quadratic equation, known as the roots of the equation, can be
      calculated by substituting the values of $a$, $b$, and $c$ into the
      following two formulas:
      + If the value of $b2–4ac$, called the discriminant, is less
	than zero, the roots of the equation, $x_1$ and $x_2$ , are imaginary
	numbers.
      Write a program to solve a quadratic equation.The program should allow
      the user to enter the values for a, b, and c. If the discriminant is less
      than zero, a message should be displayed that the roots are imaginary;
      otherwise, the program should then proceed to calculate and display the
      two roots of the equation. (Note: Be certain to make use of the squareRoot
      function that you developed in this chapter.)

* TODO Assessment

  1. *Due Date: Jun 17, Thursday*
  2. Grading
	\begin{equation}
	grade = \begin{cases}
		x\mbox{ if } x \leq 10\mbox{ where }x = \frac{A + 2B + 3C}{18} \times 10 \geq 10\\
		10\mbox{ otherwise }
		\end{cases}
	\end{equation}
	\begin{equation}
	extra = 1 if C \geq 4
	\end{equation}
  4. Assesment Reference.

#+ATTR_LATEX: :environment tabularx :width \textwidth :align |p{1in}|X|X|X|X|X|
      |----------------------+----------------------+----------------------+----------------------+----------------------+----------------------|
      | <20>                 | <20>                 | <20>                 | <20>                 | <20>                 | <20>                 |
      |                      | 1. Unnaceptable      | 2. Basic             | 3.  Proficient       | 4.  Exceed Expectations | 5. Distinguished     |
      |----------------------+----------------------+----------------------+----------------------+----------------------+----------------------|
      | A. Due Date          | Sent one week later. | Sent three days later. | Sent on time.        | Sent two days before. | Sent four days before. |
      |----------------------+----------------------+----------------------+----------------------+----------------------+----------------------|
      | B. Creativity        | Submission is a copy of another application on the Internet | Submission is a copy of another application on the Internet with slight changes. | Submission is an original idea with some elements from tutorials and examples | Submission is complety original and low on complexity | Submission is an original idea and high on complexity |
      |----------------------+----------------------+----------------------+----------------------+----------------------+----------------------|
      | C. Quality of Response | Application is incomplete and only covers one element of the requested conditions | Applitaction is not fully complete | Application is complete | Application includes some extra low complexity elements | Application includes extra high complexity elements |
      |----------------------+----------------------+----------------------+----------------------+----------------------+----------------------|
